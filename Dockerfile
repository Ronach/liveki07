FROM python:3.8-buster

LABEL maintainer="Le Ny Loann <chickenrasta@hotmail.com>" \
      application="api+redis conteneurisés à déployer sur cluster k8s via Helm"

WORKDIR /usr/src/app

COPY requirements.txt .
COPY main.py /usr/src/app

RUN mkdir -p /usr/src/app /home/apiuser && \
    useradd -s /bin/bash -d /home/apiuser apiuser && \
    chown -R apiuser /usr/src/app /home/apiuser && \
    pip3 install --no-cache-dir -r requirements.txt

USER apiuser

EXPOSE 8080

#CMD ["python3" "main.py"]
CMD python3 main.py
